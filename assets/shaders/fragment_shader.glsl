#version 330 core

uniform vec3 light_dir;
in vec3 frag_col;
in vec3 frag_pos;
in vec3 normal;
out vec3 col;

void main() {
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(-light_dir);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuse = diff * frag_col;
	col = diffuse;
}
