#version 330 core
layout(location = 0) in vec3 vert_pos;
layout(location = 1) in vec3 vert_norm;

uniform vec3 col;
uniform vec3 light_col;
uniform mat4 model;
uniform mat4 view;
uniform mat4 proj;

out vec3 frag_pos;
out vec3 frag_col;
out vec3 normal;

void main() {
	gl_Position = proj * view * model * vec4(vert_pos, 1);
	frag_pos = vec3(model * vec4(vert_pos, 1.0));
	frag_col = col * light_col;
	normal = mat3(transpose(inverse(model))) * vert_norm;
}
