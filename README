=============
*** TTT3D ***
=============
TTT3D is a 3D tic-tac-toe game for class. This project was implemented
in OpenGL 3.3, and uses several different libraries which were used to
simplify the development.

# How to Play
-------------
The objective of the game is to place three pieces of a kind in a row of
three. When entering the game there will be a piece hovering over the
board in green. This is the temporary piece you place on the board in
order to choose which square to put it in. You can control this piece
with the following inputs:

  - Arrow Keys: move the piece across the board.
  - Spacebar: Place the piece below.

If the piece is red, this means that another piece is currently below,
and you cannot place another piece there.

# Building
----------
## Requirements
The following requirements are necessary to build this project:
 - C++14 compatible compiler (GCC is recommended)
 - CMake 2.8 or greater
 - OpenGL 3.3 or greater
 - GLEW
 - GLUT
 - GLM
 - AssImp (used for model importing)
 - SDL2 (used for window and input management)

## Compiling
To compile run the following commands from the root directory of the
project:

  cd build/
  cmake ..
  make

By default this will create a release build of the program with compiler
optimizations. To compile with debug flags, append the flag
`-DCMAKE_BUILD_TYPE=debug' to the `cmake' command above.

# License
---------
The contents of the directory `assets/models/' belong to the public
domain. All other files of this project are licensed with the GNU Affero
General Publice License (see `LICENSE' file).
