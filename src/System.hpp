/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Logger.hpp"
#include "Shader.hpp"
#include "AssetManager.hpp"
#include <SDL2/SDL.h>
#include <memory>
#include <map>
#include <GL/glew.h>
#include <GL/gl.h>
#include "Camera.hpp"
class System {
public:
	System();
	~System();
	void run();

	static std::unique_ptr<Logger> logger;

private:
	void syncInputs();
	int board[3][3];
	bool xturn;
	int tempx;
	int tempy;
	AssetManager asset_mngr;
	SDL_Window *window;
	SDL_GLContext glcontext;
	std::map<std::string, Shader> shaders;
	Camera cam;
	bool running;
	int CheckWin();
	void handleKey(SDL_Keysym key);
};
