/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <fstream>
#include <vector>

/**
 * @brief Class used to write lines to the log files as well as the
 * standard output.
 */
class Logger {
public:
	/**
	 * @brief Initialize the logger.
	 *
	 * @param path Path of the log file.
	 */
	Logger(const std::string &path);
	~Logger();
	void write(const std::string &message);
	void writeDebug(const std::string &message);
	void writeWarn(const std::string &message);
	void writeError(const std::string &message);

	inline bool getDebug() const { return debug; }
	inline void setDebug(bool debug) { this->debug = debug; }

private:
	std::vector<std::string> log;
	std::ofstream logFile;

	bool debug;
};
