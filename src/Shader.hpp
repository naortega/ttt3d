/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <GL/glew.h>
#include <GL/gl.h>
#include <glm/glm.hpp>

class Shader {
public:
	Shader(const std::string &vShaderPath,
			const std::string &fShaderPath);

	inline void use() {
		glUseProgram(id);
	}
	inline void setBool(const std::string &name,
			bool value) const
	{
		glUniform1i(glGetUniformLocation(id, name.c_str()),
				static_cast<int>(value));
	}
	inline void setInt(const std::string &name,
			bool value) const
	{
		glUniform1i(glGetUniformLocation(id, name.c_str()),
				value);
	}
	inline void setFloat(const std::string &name,
			bool value) const
	{
		glUniform1f(glGetUniformLocation(id, name.c_str()),
				value);
	}
	inline void setVec3(const std::string &name,
			const glm::vec3 &v)
	{
		glUniform3f(glGetUniformLocation(id, name.c_str()),
				v.x, v.y, v.z);
	}
	inline void setMat4(const std::string &name,
			const glm::mat4 &m)
	{
		glUniformMatrix4fv(glGetUniformLocation(id, name.c_str()),
				1, GL_FALSE, &m[0][0]);
	}
	inline unsigned int getId() const {
		return id;
	}

private:
	unsigned int id;
};
