/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include "Shader.hpp"

#include <vector>
#include <glm/glm.hpp>

struct Color {
	float r;
	float g;
	float b;
};

struct Vertex {
	glm::vec3 position;
	glm::vec3 normal;
};

class Model {
public:
	Model(const std::vector<struct Vertex> &vertices,
			const std::vector<unsigned int> &indices);
	void draw(const Shader &shader);

	inline void setColor(float r, float g, float b) {
		color = { r, g, b };
	}

private:
	struct Color color;
	std::vector<struct Vertex> vertices;
	std::vector<unsigned int> indices;
	unsigned int vao, vbo, ebo;
};
