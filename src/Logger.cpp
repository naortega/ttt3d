/*
 * Copyright (C) 2018 Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 * Author: Ortega Froysa, Nicolás <nortega@themusicinnoise.net>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Logger.hpp"

#include <iostream>

Logger::Logger(const std::string &path) :
#ifdef DEBUG
	debug(true)
#else
	debug(false)
#endif
{
	logFile.open(path);
}

Logger::~Logger() {
	logFile.close();
}

void Logger::write(const std::string &message) {
	std::cout << message << std::endl;
	logFile << message << "\n";
	log.push_back(message);
}

void Logger::writeDebug(const std::string &message) {
	if(debug)
	{
		const std::string fullMsg = "[DEBUG] " + message;
		std::cout << fullMsg << std::endl;
		logFile << fullMsg << "\n";
		log.push_back(fullMsg);
	}
}

void Logger::writeWarn(const std::string &message) {
	const std::string fullMsg = "[WARN] " + message;
	std::cout << fullMsg << std::endl;
	logFile << fullMsg << "\n";
	log.push_back(fullMsg);
}

void Logger::writeError(const std::string &message) {
	const std::string fullMsg = "[ERROR] " + message;
	std::cerr << fullMsg << std::endl;
	logFile << fullMsg << "\n";
	log.push_back(fullMsg);
}
